from django.conf.urls import patterns, include, url
from django.contrib import admin
from register import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'AdressBook.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^register/', include('register.urls')),
    #url(r'^register/(?P<contact_id>\d+)/$', include('register.urls')),
    #url(r'^register/(?P<contact_id>\d+)/location/$', include('register.urls')),
    #url(r'^register/(?P<contact_id>\d+)/location/(?P<location_id>\d+)/$', include('register.urls')),
)
