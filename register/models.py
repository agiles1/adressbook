from django.db import models

# Create your models here.
class Contact(models.Model):
    firstName = models.CharField(max_length = 20)
    lastName = models.CharField(max_length = 20)
    company = models.CharField(max_length = 20)
    def __str__(self):
        return self.firstName + ' ' + self.lastName

class Location(models.Model):
    contact = models.ForeignKey(Contact)
    name = models.CharField(max_length = 20)
    def __str__(self):
        return self.name

class LocationInfo(models.Model):
    ADDRESS = 'Address'
    PHONE = 'Phone'
    MAIL = 'Mail'
    TYPE_CHOICE = ((ADDRESS, 'Address'), (PHONE, 'Phone'), (MAIL, 'Mail'))
    location = models.ForeignKey(Location)
    type = models.CharField(max_length=20, choices=TYPE_CHOICE, default=ADDRESS)
    value = models.CharField(max_length=255)







