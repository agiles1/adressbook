from django.contrib import admin
from register.models import Contact, Location, LocationInfo

# Register your models here.
admin.site.register(Contact)
admin.site.register(Location)
admin.site.register(LocationInfo)