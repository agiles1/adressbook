from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.db.models import Q
from django.template import RequestContext, loader

from register.models import Contact
from register.models import Location
from register.models import LocationInfo
from forms import ContactForm, LocationInfoFormSet, LocationForm

# Create your views here.
def index(request, contact_id=None):
    contactInstance = None

    if contact_id:
        contactInstance = get_object_or_404(Contact, pk=contact_id)


    form = ContactForm(request.POST or None, instance=contactInstance)
    if form.is_valid():

        form.save(  )
        return HttpResponseRedirect('/register/')

    if 'q' in request.GET and request.GET['q']:
        search = request.GET['q']
        contactList = Contact.objects.filter(Q(firstName__icontains=search) | Q(lastName__icontains=search)).order_by('lastName')
    else:
        contactList = Contact.objects.order_by('lastName')
    context= {'contactList': contactList,'form': form}
    return render(request,'register/index.html',context)

def editLocation(request, contact_id=None, location_id=None):
    contactInstance = get_object_or_404(Contact, pk=contact_id)
    locationInstance = Location()
    locationInstance.contact=contactInstance

    if location_id:
        locationInstance = get_object_or_404(Location, pk=location_id)


    form = LocationForm(request.POST or None, instance=locationInstance)
    formSet = LocationInfoFormSet(request.POST or None, instance=locationInstance)
    if form.is_valid():
        loc = form.save(commit=False)
        if formSet.is_valid():
            loc.save()
            formSet.save()
            return HttpResponseRedirect(request.path)


    locationList = Location.objects.all()

    context = {'locationList': locationList,'form':form, 'formSet': formSet, 'contactInstance': contactInstance}

    return render(request, 'register/location.html', context)