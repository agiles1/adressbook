from django.conf.urls import patterns, url
from register import views


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'AdressBook.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', views.index, name = 'index'),
    url(r'^(?P<contact_id>\d+)/$', views.index, name = 'edit_contact'),
    url(r'^(?P<contact_id>\d+)/location/$', views.editLocation, name = 'locations'),
    url(r'^(?P<contact_id>\d+)/location/(?P<location_id>\d+)/$', views.editLocation, name = 'edit_locations'),
)
