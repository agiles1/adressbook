__author__ = 'agiles'

from django.forms import ModelForm
from django.forms.models import inlineformset_factory
from register.models import Contact, Location, LocationInfo

class ContactForm(ModelForm):
    class Meta:
        model = Contact

class LocationForm(ModelForm):
    class Meta:
        model = Location
        fields = ['name']

LocationInfoFormSet = inlineformset_factory(Location,LocationInfo, can_delete=False, extra=1)
